package inacap.test.holamundoiei4d.controlador;

import android.content.ContentValues;
import android.content.Context;

import inacap.test.holamundoiei4d.modelo.sqlite.HolaMundoDBContract;
import inacap.test.holamundoiei4d.modelo.sqlite.UsuariosModel;


/**
 * Created by 19416173-5 on 01/09/2017.
 */

public class UsuariosController {
    private UsuariosModel usuariosModel;

    public UsuariosController(Context context){
        this.usuariosModel = new UsuariosModel(context);
    }

    public void crearUsuario(String username, String password1, String password2)throws Exception{

        if (!password1.equals(password2)) {
            //si las contraseñas no coinciden lanzamos un exception
            //Este debe ser manejado por la Activity
            throw new Exception("Contraseñas no coinciden");

        }
        // Llamar UsuarioModel para agregar un usuario a la base de datos
        ContentValues usuario = new ContentValues();
        usuario.put(HolaMundoDBContract.HolaMundoUsuarios.COLUMN_NAME_USERNAME, username);
        usuario.put(HolaMundoDBContract.HolaMundoUsuarios.COLUMN_NAME_PASSWORD, password1);
        //usuario.put(HolaMundoDBContract.HolaMundoUsuarios.COLUMN_NAME_PASSWORD, password2);

        this.usuariosModel.crearUsuario(usuario);
    }

    public boolean usuarioLoggin(String username, String password)throws Exception{
        ContentValues usuarios = null;
        try {
            usuarios = this.usuariosModel.obtenerUsuarioPorUsername(username);
            if (password.equals(usuarios.get(HolaMundoDBContract.HolaMundoUsuarios.COLUMN_NAME_PASSWORD))){
                return true;
            }
        } catch (Exception e) {
            switch (e.getMessage()){
                case "Username no encontrado":
                    throw new Exception(e.getMessage());

            }
            return false;
        }

     return false;
    }
}
