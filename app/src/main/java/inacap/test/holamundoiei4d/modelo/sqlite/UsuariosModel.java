package inacap.test.holamundoiei4d.modelo.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by mitlley on 25-08-17.
 */

public class UsuariosModel {
    private HolaMundoDBHelper dbHelper;

    public UsuariosModel(Context context){
        this.dbHelper = new HolaMundoDBHelper(context);
    }

    public void crearUsuario(ContentValues usuario){
        SQLiteDatabase db = this.dbHelper.getWritableDatabase();
        db.insert(HolaMundoDBContract.HolaMundoUsuarios.TABLE_NAME, null, usuario);
    }

    public ContentValues obtenerUsuarioPorUsername(String username)throws Exception{
        //Pedimos conexion a bdd
        SQLiteDatabase db = this.dbHelper.getReadableDatabase();
        //indicar con que columnas trabajaremos (Proyeccion)
        String[] projection = {
                HolaMundoDBContract.HolaMundoUsuarios._ID,
                HolaMundoDBContract.HolaMundoUsuarios.COLUMN_NAME_USERNAME,
                HolaMundoDBContract.HolaMundoUsuarios.COLUMN_NAME_PASSWORD
        };

        //Como filtrar

        String selection = HolaMundoDBContract.HolaMundoUsuarios.COLUMN_NAME_USERNAME + " = ? LIMIT 1";
        String[] selectionArgs = {username};

        //Hacer consulta
        Cursor cur = db.query(
                HolaMundoDBContract.HolaMundoUsuarios.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        if(cur.getCount()==0){
            throw new Exception("Username no encontrado");
        }

        //Tomar el primer registro
        cur.moveToFirst();
        String cur_username = cur.getString(cur.getColumnIndex(HolaMundoDBContract.HolaMundoUsuarios.COLUMN_NAME_USERNAME));
        String cur_password = cur.getString(cur.getColumnIndex(HolaMundoDBContract.HolaMundoUsuarios.COLUMN_NAME_PASSWORD));

        ContentValues usuario = new ContentValues();
        usuario.put(HolaMundoDBContract.HolaMundoUsuarios.COLUMN_NAME_USERNAME, cur_username);
        usuario.put(HolaMundoDBContract.HolaMundoUsuarios.COLUMN_NAME_PASSWORD, cur_password);

        return usuario;
    }
}
