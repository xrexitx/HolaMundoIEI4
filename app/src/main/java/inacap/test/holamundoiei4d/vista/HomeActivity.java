package inacap.test.holamundoiei4d.vista;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import inacap.test.holamundoiei4d.MainActivity;
import inacap.test.holamundoiei4d.R;
import inacap.test.holamundoiei4d.modelo.sqlite.HolaMundoDBContract;

public class HomeActivity extends AppCompatActivity {

    public Button btnCerrarSesion;
    public TextView txtMostrarUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        this.btnCerrarSesion=(Button) findViewById(R.id.btnCerrarSesion);
        this.txtMostrarUsuario=(TextView) findViewById(R.id.txtMostrarUsuario);

        btnCerrarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sesiones = getSharedPreferences(HolaMundoDBContract.HolaMundoSesion.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sesiones.edit();
                //Guardar boolean que diga si inicio sesion
                editor.putBoolean(HolaMundoDBContract.HolaMundoSesion.FIELD_SESION, false);
                editor.commit();

                Intent i = new Intent (HomeActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        });

    }
}
